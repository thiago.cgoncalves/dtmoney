import { Container } from "./styles";
import income from "../../assets/income.svg"
import outcome from "../../assets/outcome.svg"
import total from "../../assets/total.svg"
import { useTransactions } from "../../hooks/useTransactions";

export default function Summary() {

  const { transactions } = useTransactions();

  function getValuebyTransactionType(type: string) {
    const value = transactions.reduce((accumulator, currentValue) => {
      if (currentValue.type !== type) return accumulator;
      return accumulator + currentValue.amount;
    }, 0)
    return getFormatedValue(value);
  }

  function getTotalValue() {
    const value = transactions.reduce((accumulator, currentValue) => {
      if (currentValue.type === 'withdraw') return accumulator - currentValue.amount;
      return accumulator + currentValue.amount;
    }, 0)
    return getFormatedValue(value);
  }

  function getFormatedValue(value: number) {
    return new Intl.NumberFormat('pt-PT', {
      style: 'currency',
      currency: 'EUR'
    }).format(value)
  }

  return (
    <Container>
      <div>
        <header>
          <p>Entradas</p>
          <img src={income} alt="Entradas" />
        </header>
        <strong>{getValuebyTransactionType('deposit')}</strong>
      </div>
      <div>
        <header>
          <p>Saídas</p>
          <img src={outcome} alt="Saídas" />
        </header>
        <strong>{getValuebyTransactionType('withdraw')}</strong>
      </div>
      <div className="highlight-background">
        <header>
          <p>Total</p>
          <img src={total} alt="Total" />
        </header>
        <strong>{getTotalValue()}</strong>
      </div>
    </Container>
  )
}