import { useEffect, useState } from "react";
import { Container } from "./styles";
import api from "../../services/api";
import { useTransactions } from "../../hooks/useTransactions";


export default function TransactionsTable() {
  const { transactions } = useTransactions();
  
  function getFormatedValue(value: number) {
    return new Intl.NumberFormat('pt-PT', {
      style: 'currency',
      currency: 'EUR'
    }).format(value)
  }

  return (
    <Container>
      <table>
        <thead>
          <tr>
            <th>Título</th>
            <th>Valor</th>
            <th>Categoria</th>
            <th>Data</th>
          </tr>
        </thead>
        <tbody>
          {transactions && transactions.map((transaction, index) => (
            <tr key={index}>
              <td>{transaction.title}</td>
              <td className={transaction.type}>
                {transaction.type === 'deposit' ? '' : '-'}
                {getFormatedValue(transaction.amount)}
              </td>
              <td>{transaction.category}</td>
              <td>{new Intl.DateTimeFormat('pt-PT').format(
                new Date(transaction.createdAt)
              )}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </Container>
  )
}