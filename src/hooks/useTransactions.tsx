import { ReactNode, createContext, useContext, useEffect, useState } from "react";
import api from "../services/api";

interface Transaction {
  id: number
  title: string;
  amount: number;
  type: string;
  category: string;
  createdAt: string;
}

type TransactionInput = Omit<Transaction, 'id' | 'createdAt'>


interface TransactionsProviderProps {
  children: ReactNode
}

interface TransctionsContextData {
  transactions: Transaction[];
  createTransaction: (transaction: TransactionInput) => Promise<void>;
}

export const TransactionsContext = createContext<TransctionsContextData>({} as TransctionsContextData);

export function TransactionsProvider(props: TransactionsProviderProps) {
  const [ transactions, setTransactions ] = useState<Transaction[]>([]);

  useEffect(() => {
    const getInitialData = async () => {
      const response = await api('transactions');
      setTransactions(response.data.transactions);
    }
    getInitialData();
  }, [])

  async function createTransaction(transactionInput: TransactionInput) {
    const response = await api.post('/transactions', {
      ...transactionInput,
      createdAt: new Date()
    });
    const { transaction } = response.data;
    setTransactions([
      ...transactions,
      transaction
    ])
  }

  return (
    <TransactionsContext.Provider value={{ transactions, createTransaction }}>
      {props.children}
    </TransactionsContext.Provider>
  )
}

export function useTransactions() {
  return useContext(TransactionsContext);
}
