import React from 'react';
import ReactDOM from 'react-dom/client';
import { Model, createServer } from 'miragejs';
import App from './App';

createServer({
  models: {
    transaction: Model
  },
  seeds(server) {
    server.db.loadData({
      transactions: [
        {
          id: 1,
          title: 'Permanent Job',
          amount: 4300,
          category: 'Work',
          type: 'deposit',
          createdAt: new Date('2024-01-10 10:00:00')
        },
        {
          id: 2,
          title: 'Rent',
          amount: 1800,
          category: 'House',
          type: 'withdraw',
          createdAt: new Date('2024-01-10 10:00:00')
        }
      ]
    })
  },
  routes() {
    this.namespace = 'api';
    this.get('/transactions', () => {
      return this.schema.all('transaction')
    })
    this.post('/transactions', (schema, request) => {
      const data = JSON.parse(request.requestBody);
      return schema.create('transaction', data);
    })
  }
})

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
